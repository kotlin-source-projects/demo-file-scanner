package com.example.validationform.draw

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView


class StarCanvas(context: Context) : View(context) {
    private val mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var layout: LinearLayout? = null

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        canvas.drawRGB(0, 0, 0)
        val width = width
        val height = height
        val brush = Paint()
        brush.setARGB(255, 255, 255, 255)
        for (x in 1..10000) {
            val alex = (Math.random() * width).toFloat()
            val aley = (Math.random() * height).toFloat()
            canvas.drawPoint(alex, aley, brush)
        }
        brush.setARGB(255, 51, 94, 255)
        for (x in 1..10000) {
            val alex = (Math.random() * width).toFloat()
            val aley = (Math.random() * height).toFloat()
            canvas.drawPoint(alex, aley, brush)
        }
        brush.setARGB(255, 255, 184, 51)
        for (x in 1..10000) {
            val alex = (Math.random() * width).toFloat()
            val aley = (Math.random() * height).toFloat()
            canvas.drawPoint(alex, aley, brush)
        }
    }
}