package com.example.validationform.components.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.validationform.R

class PollDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage(R.string.titlePollDialog)
                .setPositiveButton(R.string.likeApp,
                    DialogInterface.OnClickListener { principalDialog, principalId ->
                        Toast.makeText(context, "Gracias por tu aprobacion " + String(Character.toChars(0x1F369)), Toast.LENGTH_LONG).show()
                    })
                .setNegativeButton(R.string.dislikeApp,
                    DialogInterface.OnClickListener { dialog, id ->
                        Toast.makeText(context, "Seguiremos trabajando para mejorar " + String(Character.toChars(0x1F369)), Toast.LENGTH_LONG).show()
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}