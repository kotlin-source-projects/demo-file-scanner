package com.example.validationform.client.dto

class PathEnableDTO {
    var internal: String
    var external: String

    constructor() {
        this.internal = ""
        this.external = ""
    }
}