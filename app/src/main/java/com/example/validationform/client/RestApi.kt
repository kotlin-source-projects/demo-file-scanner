package com.example.validationform.client

import com.example.validationform.client.dto.PathEnableDTO
import com.example.validationform.client.dto.UploadDTO
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


interface RestApi {
    @Multipart
    @POST("upload")
    fun uploadFile(@Part("internal") internal: String,
                   @Part("external")  external: String,
                   @Part file: MultipartBody.Part): Call<UploadDTO>
}