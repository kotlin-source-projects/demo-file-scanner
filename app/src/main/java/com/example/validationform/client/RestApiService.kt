package com.example.validationform.client

import com.example.validationform.client.dto.UploadDTO
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestApiService {
    fun uploadFile(internal: String, external: String, fileData: MultipartBody.Part, onResult: (UploadDTO?) -> Unit) {
        val retrofit =
            ServiceBuilder.buildService(RestApi::class.java)

        retrofit.uploadFile(internal, external, fileData).enqueue(
            object : Callback<UploadDTO> {
                override fun onFailure(call: Call<UploadDTO>, t: Throwable) {
                    println("------------>>>>>>>>>> Failure" + t.message)
                    onResult(null)
                }

                override fun onResponse(call: Call<UploadDTO>, response: Response<UploadDTO>) {
                    val uploadDTO =
                        UploadDTO()
                    if(response.code() == 200){
                        uploadDTO.message = "Success: OK " + response.code()
                        println("------------>>>>>>>>>> " + uploadDTO.message)
                    }
                    if(response.code() == 400){
                        uploadDTO.message = "Bad request: NOK " + response.code()
                        println("------------>>>>>>>>>> " + uploadDTO.message)
                    }
                    if(response.code() == 404){
                        uploadDTO.message = "Not found: NOK " + response.code()
                        println("------------>>>>>>>>>> " + uploadDTO.message)
                    }
                    if(response.code() == 500){
                        uploadDTO.message = "Internal server error: NOK " + response.code()
                        println("------------>>>>>>>>>> " + uploadDTO.message)
                    }
                    onResult(uploadDTO)
                }
            }
        )
    }
}